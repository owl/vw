# vw
A Vaultwarden/Bitwarden[^1] client.

## Features
- It's a vw/bw client, it does the thing.
- No runtime dependencies.
- Smol compared to known alternatives.

## Non-features
- PBKDF2 auth support.
- Passphrase/password generation; pairs well with [passphrase](https://git.sr.ht/~alva/passphrase).

[^1]: No affiliation with Bitwarden, I'm just a happy customer. 
