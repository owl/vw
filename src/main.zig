const std = @import("std");
const http = std.http;
const json = std.json;
const crypto = std.crypto;
const argon2 = std.crypto.pwhash.argon2;
const request = @import("api/request.zig");
const response = @import("api/response.zig");
const Config = @import("config.zig").Config;

const json_stringify_options = json.StringifyOptions{
    .whitespace = .minified,
};

const server = "https://api.bitwarden.com";
const id_server = "https://identity.bitwarden.com";

var scratch: [0x1000]u8 = undefined;

pub fn main() !void {
    var gpa = std.heap.GeneralPurposeAllocator(.{}){};

    defer {
        if (gpa.deinit() != .ok)
            @panic("leak!");
    }

    const allo = gpa.allocator();
    const conf = try Config.parseConfigAlloc(allo);
    defer conf.deinit();

    // do prelogin
    var hc = http.Client{ .allocator = allo };
    defer hc.deinit();
    try hc.ca_bundle.rescan(allo);

    const resp = try prelogin(allo, &hc, conf.value.email);
    defer resp.deinit();

    const pw = try getPin("master password", allo, conf.value.pinentry);
    defer allo.free(pw);

    const master_key = try deriveMasterKey(
        allo,
        pw,
        conf.value.email,
        .{
            .t = resp.value.kdfIterations,
            // zig argon2 uses KiB, *warden uses MiB.
            .m = resp.value.kdfMemory << 10,
            .p = resp.value.kdfParallelism,
        },
    );
    const b64e = std.base64.Base64Encoder.init(std.base64.standard_alphabet_chars, '=');

    // create hashed password
    try crypto.pwhash.pbkdf2(
        scratch[0..32],
        &master_key,
        pw,
        1,
        crypto.auth.hmac.sha2.HmacSha256,
    );

    const dst = try allo.alloc(u8, b64e.calcSize(32));
    defer allo.free(dst);

    const hashed_pw = b64e.encode(dst, scratch[0..32]);

    std.debug.print("hashed pw: {s}\n", .{hashed_pw});

    _ = try requestToken(allo, conf.value, hashed_pw, &hc, null);
}

/// caller owns return value
fn prelogin(
    allocator: std.mem.Allocator,
    http_client: *std.http.Client,
    email: []const u8,
) !json.Parsed(response.Prelogin) {
    // först ska man registrera sig.
    const prelog_uri = try std.Uri.parse(server ++ "/accounts/prelogin");
    const prelog = request.Prelogin{
        .email = email,
    };

    var fbw = std.io.fixedBufferStream(&scratch);

    try json.stringify(prelog, json_stringify_options, fbw.writer());
    const prelog_json = fbw.getWritten();

    var headers = std.http.Headers.init(allocator);
    defer headers.deinit();

    try headers.append("Content-Type", "application/json");

    var req = try http_client.open(
        .POST,
        prelog_uri,
        headers,
        .{},
    );
    defer req.deinit();

    req.transfer_encoding = .chunked;

    try req.send(.{});
    try req.writeAll(prelog_json);
    try req.finish();
    try req.wait();

    var output = std.ArrayList(u8).init(allocator);
    defer output.deinit();

    while (true) {
        const size = try req.reader().read(&scratch);
        if (size == 0) break;
        try output.appendSlice(scratch[0..size]);
    }

    const resp = try json.parseFromSlice(
        response.Prelogin,
        allocator,
        output.items,
        .{ .ignore_unknown_fields = true },
    );

    if (resp.value.kdf == .pbkdf2) {
        @panic("PBKDF2 is not supported");
    }

    std.debug.print(
        "headers: {any}\nbody: {any}\n",
        .{ req.response.headers, resp },
    );

    return resp;
}

const Sha256 = crypto.hash.sha2.Sha256;

fn deriveMasterKey(
    allocator: std.mem.Allocator,
    master_pw: []const u8,
    email: []const u8,
    argon_params: argon2.Params,
) !SymmetricCryptoKey {
    var key: SymmetricCryptoKey = undefined;
    var salt: [Sha256.digest_length]u8 = undefined;

    Sha256.hash(email, &salt, .{});

    try argon2.kdf(
        allocator,
        &key,
        master_pw,
        &salt,
        argon_params,
        .argon2id,
    );

    return key;
}

fn requestToken(
    allocator: std.mem.Allocator,
    config: Config,
    hashed_pw: []const u8,
    http_client: *std.http.Client,
    totp: ?[]const u8,
) ![]const u8 {
    const token_uri = try std.Uri.parse(id_server ++ "/connect/token");
    var token = request.AuthToken{
        .grant_type = "client_credentials",
    };

    var headers = std.http.Headers.init(allocator);
    defer headers.deinit();

    const b64e = std.base64.Base64Encoder.init(std.base64.standard_alphabet_chars, null);
    const email_b64 = b64e.encode(&scratch, config.email);

    try headers.append("Content-Type", "application/x-www-form-urlencoded");
    try headers.append("Auth-Email", email_b64);

    var req = try http_client.open(
        .POST,
        token_uri,
        headers,
        .{},
    );
    defer req.deinit();

    req.transfer_encoding = .chunked;

    try req.send(.{});

    switch (config.grant_type) {
        .client_credentials => |cc| {
            std.log.debug("client_credentials flow {s} {s}", .{ cc.client_id, cc.client_secret });
            try writeUrlEncodedParam(req.writer(), "grant_type", "client_credentials");
            try writeUrlEncodedParam(req.writer(), "client_id", cc.client_id);
            try writeUrlEncodedParam(req.writer(), "client_secret", cc.client_secret);
            try writeUrlEncodedParam(req.writer(), "scope", token.scope);
            try writeUrlEncodedParam(req.writer(), "deviceName", token.deviceName);
            try writeUrlEncodedParam(req.writer(), "deviceType", token.deviceType);
            try writeUrlEncodedParam(req.writer(), "deviceIdentifier", token.deviceIdentifier);
        },
        .password => |pw| {
            std.log.debug("password flow {s} {s}", .{ pw.username, hashed_pw });
            try writeUrlEncodedParam(req.writer(), "grant_type", "password");
            try writeUrlEncodedParam(req.writer(), "client_id", "desktop");
            try writeUrlEncodedParam(req.writer(), "username", pw.username);
            try writeUrlEncodedParam(req.writer(), "password", hashed_pw);
            try writeUrlEncodedParam(req.writer(), "scope", "api offline_access");
            try writeUrlEncodedParam(req.writer(), "deviceName", token.deviceName);
            try writeUrlEncodedParam(req.writer(), "deviceType", token.deviceType);
            try writeUrlEncodedParam(req.writer(), "deviceIdentifier", token.deviceIdentifier);
        },
    }

    if (totp) |_2fa| {
        token.twoFactorProvider = 0;
        token.twoFactorToken = _2fa;
        token.twoFactorRemember = 1;

        try writeUrlEncodedParam(req.writer(), "twoFactorProvider", "0");
        try writeUrlEncodedParam(req.writer(), "twoFactorToken", _2fa);
        try writeUrlEncodedParam(req.writer(), "twoFactorRemember", "1");
    }

    try req.finish();
    try req.wait();

    var output = std.ArrayList(u8).init(allocator);
    defer output.deinit();

    while (true) {
        const size = try req.reader().read(&scratch);
        if (size == 0) break;
        try output.appendSlice(scratch[0..size]);
    }

    const js = try std.json.parseFromSlice(
        response.Token,
        allocator,
        output.items,
        .{
            .ignore_unknown_fields = true,
        },
    );
    defer js.deinit();

    if (js.value.TwoFactorProviders) |tfp| {
        for (tfp) |t| {
            std.log.debug("two factor required {s}; trying again!", .{t});
        }
        const _2fa = try getPin("totp", allocator, config.pinentry);
        defer allocator.free(_2fa);
        std.log.debug("totp: {s}", .{_2fa});

        return requestToken(allocator, config, hashed_pw, http_client, _2fa);
    } else if (js.value.@"error") |e| {
        std.log.debug("token error: {s} {?s}", .{ e, js.value.error_description });
    }

    const resp = try json.parseFromSlice(
        response.Token,
        allocator,
        output.items,
        .{ .ignore_unknown_fields = true },
    );
    defer resp.deinit();
    std.log.debug("done, access_token: {?s}", .{resp.value.access_token});

    return output.items;
}

// TODO: use std.Uri.write*
fn writeUrlEncodedParam(
    writer: anytype,
    key: []const u8,
    val: []const u8,
) !void {
    try writer.print("{s}=", .{key});
    try std.Uri.writeEscapedString(writer, val);
    try writer.writeByte('&');
}

pub const SymmetricCryptoKey = [32]u8;

fn getPin(
    comptime prompt: []const u8,
    allocator: std.mem.Allocator,
    cmd: []const []const u8,
) ![]const u8 {
    var cp = std.ChildProcess.init(cmd, allocator);
    cp.stdin_behavior = .Pipe;
    cp.stdout_behavior = .Pipe;
    try cp.spawn();
    try cp.stdin.?.writer().writeAll("SETPROMPT " ++ prompt ++ "\nGETPIN\nBYE\n");
    const out = try cp.stdout.?.readToEndAlloc(allocator, 0x1000);
    defer allocator.free(out);
    var it = std.mem.split(u8, out, "\n");
    var ok_line = it.next() orelse return error.PinEntryProgramError;
    std.debug.assert(std.mem.startsWith(u8, ok_line, "OK"));
    ok_line = it.next() orelse return error.PinEntryProgramError;
    std.debug.assert(std.mem.startsWith(u8, ok_line, "OK"));
    const pin_line = it.next() orelse return error.PinEntryProgramError;
    std.debug.assert(std.mem.startsWith(u8, pin_line, "D "));
    std.log.debug("getpin: `{s}` (TM4)", .{pin_line[2..]});
    _ = try cp.wait();
    return allocator.dupe(u8, pin_line[2..]);
}
