const std = @import("std");
const known_folders = @import("known-folders");

pub const Config = struct {
    email: []const u8,
    pinentry: []const []const u8,
    grant_type: GrantType,

    pub fn parseConfigAlloc(allocator: std.mem.Allocator) !std.json.Parsed(@This()) {
        const dir = try known_folders.getPath(allocator, .roaming_configuration) orelse {
            std.log.err("couldn't find config directory", .{});
            std.os.exit(1);
        };
        defer allocator.free(dir);
        const path = try std.fs.path.join(
            allocator,
            &[_][]const u8{ dir, "vw", "config.json" },
        );
        defer allocator.free(path);

        const conf_file = try std.fs.openFileAbsolute(path, .{});
        defer conf_file.close();

        var buf: [0x1000]u8 = undefined;
        const n_conf_bytes = try conf_file.readAll(&buf);

        const conf = try std.json.parseFromSlice(
            Config,
            allocator,
            buf[0..n_conf_bytes],
            .{
                .ignore_unknown_fields = true,
                .allocate = .alloc_always,
            },
        );

        return conf;
    }
};

pub const GrantType = union(enum) {
    client_credentials: struct {
        client_id: []const u8,
        client_secret: []const u8,
    },
    password: struct {
        username: []const u8,
    },
};
