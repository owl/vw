const builtin = @import("builtin");

// json
pub const Prelogin = struct {
    email: []const u8,
};

// url params
pub const AuthToken = struct {
    grant_type: []const u8,
    username: ?[]const u8 = null,
    // hashed pw
    password: ?[]const u8 = null,
    scope: []const u8 = "api",
    client_id: []const u8 = "connector", // seen in bitwarden/jslib
    deviceType: []const u8 = deviceType(),
    deviceName: []const u8 = "vw-" ++ @tagName(builtin.os.tag),
    // TODO: don't hardcode
    deviceIdentifier: []const u8 = "d5d7e666-2cce-4d62-8482-c8fa1eb22552",
    twoFactorProvider: ?i8 = null,
    twoFactorToken: ?[]const u8 = null,
    twoFactorRemember: ?u1 = null,
};

fn deviceType() []const u8 {
    // enum strings from https://github.com/bitwarden/server/blob/b19628c6f85a2cd5f1950ac222ba14840a88894d/src/Core/Enums/DeviceType.cs.
    return switch (builtin.os.tag) {
        .windows => "6",
        .macos => "7",
        .linux => "8",
        else => "14",
    };
}
