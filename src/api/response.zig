const std = @import("std");

// Response structs.
pub const Prelogin = struct {
    // {"kdf":1,"kdfIterations":3,"kdfMemory":64,"kdfParallelism":4}
    kdf: enum(u8) {
        pbkdf2 = 0,
        argon2id = 1,
    },
    kdfIterations: u32,
    kdfMemory: u32,
    kdfParallelism: u24,
};

pub const Token = struct {
    @"error": ?[]const u8 = null,
    error_description: ?[]const u8 = null,
    TwoFactorProviders: ?[]const []const u8 = null,
    TwoFactorProviders2: ?std.json.Value = null,
    MasterPasswordPolicy: ?std.json.Value = null,
    access_token: ?[]const u8 = null,
    expires_in: ?usize = null,
    token_type: ?TokenType = null,
    scope: ?[]const u8 = null,
    PrivateKey: ?[]const u8 = null,
    //MasterPasswordPolicy: void,
    ForcePasswordReset: ?bool = null,
    ResetMasterPassword: ?bool = null,
    Kdf: ?enum(u8) {
        pbkdf2 = 0,
        argon2id = 1,
    } = null,
    KdfIterations: ?u32 = null,
    KdfMemory: ?u32 = null,
    KdfParallelism: ?u24 = null,
    //UserDecryptionOptions":{"HasMasterPassword":true,"Object":"userDecryptionOptions"}}
};

pub const TwoFactorProvider = struct {
    challenge: []const u8,
    timeout: usize,
    rpId: []const u8,
    allowCredentials: []const Cred,
    userVerification: []const u8,
    extensions: void,
    status: []const u8,
    errorMessage: []const u8,
};

pub const Cred = struct {
    type: []const u8,
    id: []const u8,
};

pub const TokenType = enum {
    Bearer,
};
